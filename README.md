# Arduino as ISP for platformio
The original ArduinoISP code provided with the Arduino IDE does not work in platformio. This version is good to go. No code changes, only the functions sorted properly.